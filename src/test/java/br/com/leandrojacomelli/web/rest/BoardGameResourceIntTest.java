package br.com.leandrojacomelli.web.rest;

import br.com.leandrojacomelli.RpgApp;

import br.com.leandrojacomelli.domain.BoardGame;
import br.com.leandrojacomelli.repository.BoardGameRepository;
import br.com.leandrojacomelli.service.BoardGameService;
import br.com.leandrojacomelli.repository.search.BoardGameSearchRepository;
import br.com.leandrojacomelli.service.dto.BoardGameDTO;
import br.com.leandrojacomelli.service.mapper.BoardGameMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BoardGameResource REST controller.
 *
 * @see BoardGameResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RpgApp.class)
public class BoardGameResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String UPDATED_SUMMARY = "BBBBB";

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private BoardGameRepository boardGameRepository;

    @Inject
    private BoardGameMapper boardGameMapper;

    @Inject
    private BoardGameService boardGameService;

    @Inject
    private BoardGameSearchRepository boardGameSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restBoardGameMockMvc;

    private BoardGame boardGame;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BoardGameResource boardGameResource = new BoardGameResource();
        ReflectionTestUtils.setField(boardGameResource, "boardGameService", boardGameService);
        this.restBoardGameMockMvc = MockMvcBuilders.standaloneSetup(boardGameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BoardGame createEntity(EntityManager em) {
        BoardGame boardGame = new BoardGame()
                .name(DEFAULT_NAME)
                .summary(DEFAULT_SUMMARY)
                .startDate(DEFAULT_START_DATE);
        return boardGame;
    }

    @Before
    public void initTest() {
        boardGameSearchRepository.deleteAll();
        boardGame = createEntity(em);
    }

    @Test
    @Transactional
    public void createBoardGame() throws Exception {
        int databaseSizeBeforeCreate = boardGameRepository.findAll().size();

        // Create the BoardGame
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(boardGame);

        restBoardGameMockMvc.perform(post("/api/board-games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(boardGameDTO)))
                .andExpect(status().isCreated());

        // Validate the BoardGame in the database
        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeCreate + 1);
        BoardGame testBoardGame = boardGames.get(boardGames.size() - 1);
        assertThat(testBoardGame.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBoardGame.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testBoardGame.getStartDate()).isEqualTo(DEFAULT_START_DATE);

        // Validate the BoardGame in ElasticSearch
        BoardGame boardGameEs = boardGameSearchRepository.findOne(testBoardGame.getId());
        assertThat(boardGameEs).isEqualToComparingFieldByField(testBoardGame);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = boardGameRepository.findAll().size();
        // set the field null
        boardGame.setName(null);

        // Create the BoardGame, which fails.
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(boardGame);

        restBoardGameMockMvc.perform(post("/api/board-games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(boardGameDTO)))
                .andExpect(status().isBadRequest());

        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSummaryIsRequired() throws Exception {
        int databaseSizeBeforeTest = boardGameRepository.findAll().size();
        // set the field null
        boardGame.setSummary(null);

        // Create the BoardGame, which fails.
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(boardGame);

        restBoardGameMockMvc.perform(post("/api/board-games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(boardGameDTO)))
                .andExpect(status().isBadRequest());

        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = boardGameRepository.findAll().size();
        // set the field null
        boardGame.setStartDate(null);

        // Create the BoardGame, which fails.
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(boardGame);

        restBoardGameMockMvc.perform(post("/api/board-games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(boardGameDTO)))
                .andExpect(status().isBadRequest());

        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBoardGames() throws Exception {
        // Initialize the database
        boardGameRepository.saveAndFlush(boardGame);

        // Get all the boardGames
        restBoardGameMockMvc.perform(get("/api/board-games?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(boardGame.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
                .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())));
    }

    @Test
    @Transactional
    public void getBoardGame() throws Exception {
        // Initialize the database
        boardGameRepository.saveAndFlush(boardGame);

        // Get the boardGame
        restBoardGameMockMvc.perform(get("/api/board-games/{id}", boardGame.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(boardGame.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBoardGame() throws Exception {
        // Get the boardGame
        restBoardGameMockMvc.perform(get("/api/board-games/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBoardGame() throws Exception {
        // Initialize the database
        boardGameRepository.saveAndFlush(boardGame);
        boardGameSearchRepository.save(boardGame);
        int databaseSizeBeforeUpdate = boardGameRepository.findAll().size();

        // Update the boardGame
        BoardGame updatedBoardGame = boardGameRepository.findOne(boardGame.getId());
        updatedBoardGame
                .name(UPDATED_NAME)
                .summary(UPDATED_SUMMARY)
                .startDate(UPDATED_START_DATE);
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(updatedBoardGame);

        restBoardGameMockMvc.perform(put("/api/board-games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(boardGameDTO)))
                .andExpect(status().isOk());

        // Validate the BoardGame in the database
        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeUpdate);
        BoardGame testBoardGame = boardGames.get(boardGames.size() - 1);
        assertThat(testBoardGame.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBoardGame.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testBoardGame.getStartDate()).isEqualTo(UPDATED_START_DATE);

        // Validate the BoardGame in ElasticSearch
        BoardGame boardGameEs = boardGameSearchRepository.findOne(testBoardGame.getId());
        assertThat(boardGameEs).isEqualToComparingFieldByField(testBoardGame);
    }

    @Test
    @Transactional
    public void deleteBoardGame() throws Exception {
        // Initialize the database
        boardGameRepository.saveAndFlush(boardGame);
        boardGameSearchRepository.save(boardGame);
        int databaseSizeBeforeDelete = boardGameRepository.findAll().size();

        // Get the boardGame
        restBoardGameMockMvc.perform(delete("/api/board-games/{id}", boardGame.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean boardGameExistsInEs = boardGameSearchRepository.exists(boardGame.getId());
        assertThat(boardGameExistsInEs).isFalse();

        // Validate the database is empty
        List<BoardGame> boardGames = boardGameRepository.findAll();
        assertThat(boardGames).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchBoardGame() throws Exception {
        // Initialize the database
        boardGameRepository.saveAndFlush(boardGame);
        boardGameSearchRepository.save(boardGame);

        // Search the boardGame
        restBoardGameMockMvc.perform(get("/api/_search/board-games?query=id:" + boardGame.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(boardGame.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())));
    }
}
