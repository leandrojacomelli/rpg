package br.com.leandrojacomelli.web.rest;

import br.com.leandrojacomelli.RpgApp;

import br.com.leandrojacomelli.domain.Characterz;
import br.com.leandrojacomelli.repository.CharacterzRepository;
import br.com.leandrojacomelli.service.CharacterzService;
import br.com.leandrojacomelli.repository.search.CharacterzSearchRepository;
import br.com.leandrojacomelli.service.dto.CharacterzDTO;
import br.com.leandrojacomelli.service.mapper.CharacterzMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CharacterzResource REST controller.
 *
 * @see CharacterzResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RpgApp.class)
public class CharacterzResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_PLAYER = "AAAAA";
    private static final String UPDATED_PLAYER = "BBBBB";

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final String DEFAULT_CLAZZ = "AAAAA";
    private static final String UPDATED_CLAZZ = "BBBBB";
    private static final String DEFAULT_RACE = "AAAAA";
    private static final String UPDATED_RACE = "BBBBB";

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final Integer DEFAULT_LIFE_POINTS = 1;
    private static final Integer UPDATED_LIFE_POINTS = 2;

    private static final Integer DEFAULT_MAGIC_POINTS = 1;
    private static final Integer UPDATED_MAGIC_POINTS = 2;
    private static final String DEFAULT_ITENS = "AAAAA";
    private static final String UPDATED_ITENS = "BBBBB";
    private static final String DEFAULT_SKILLS = "AAAAA";
    private static final String UPDATED_SKILLS = "BBBBB";

    @Inject
    private CharacterzRepository characterzRepository;

    @Inject
    private CharacterzMapper characterzMapper;

    @Inject
    private CharacterzService characterzService;

    @Inject
    private CharacterzSearchRepository characterzSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCharacterzMockMvc;

    private Characterz characterz;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CharacterzResource characterzResource = new CharacterzResource();
        ReflectionTestUtils.setField(characterzResource, "characterzService", characterzService);
        this.restCharacterzMockMvc = MockMvcBuilders.standaloneSetup(characterzResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Characterz createEntity(EntityManager em) {
        Characterz characterz = new Characterz()
                .name(DEFAULT_NAME)
                .player(DEFAULT_PLAYER)
                .level(DEFAULT_LEVEL)
                .clazz(DEFAULT_CLAZZ)
                .race(DEFAULT_RACE)
                .age(DEFAULT_AGE)
                .lifePoints(DEFAULT_LIFE_POINTS)
                .magicPoints(DEFAULT_MAGIC_POINTS)
                .itens(DEFAULT_ITENS)
                .skills(DEFAULT_SKILLS);
        return characterz;
    }

    @Before
    public void initTest() {
        characterzSearchRepository.deleteAll();
        characterz = createEntity(em);
    }

    @Test
    @Transactional
    public void createCharacterz() throws Exception {
        int databaseSizeBeforeCreate = characterzRepository.findAll().size();

        // Create the Characterz
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isCreated());

        // Validate the Characterz in the database
        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeCreate + 1);
        Characterz testCharacterz = characterzs.get(characterzs.size() - 1);
        assertThat(testCharacterz.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCharacterz.getPlayer()).isEqualTo(DEFAULT_PLAYER);
        assertThat(testCharacterz.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testCharacterz.getClazz()).isEqualTo(DEFAULT_CLAZZ);
        assertThat(testCharacterz.getRace()).isEqualTo(DEFAULT_RACE);
        assertThat(testCharacterz.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testCharacterz.getLifePoints()).isEqualTo(DEFAULT_LIFE_POINTS);
        assertThat(testCharacterz.getMagicPoints()).isEqualTo(DEFAULT_MAGIC_POINTS);
        assertThat(testCharacterz.getItens()).isEqualTo(DEFAULT_ITENS);
        assertThat(testCharacterz.getSkills()).isEqualTo(DEFAULT_SKILLS);

        // Validate the Characterz in ElasticSearch
        Characterz characterzEs = characterzSearchRepository.findOne(testCharacterz.getId());
        assertThat(characterzEs).isEqualToComparingFieldByField(testCharacterz);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setName(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPlayerIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setPlayer(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setLevel(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClazzIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setClazz(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRaceIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setRace(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = characterzRepository.findAll().size();
        // set the field null
        characterz.setAge(null);

        // Create the Characterz, which fails.
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);

        restCharacterzMockMvc.perform(post("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isBadRequest());

        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCharacterzs() throws Exception {
        // Initialize the database
        characterzRepository.saveAndFlush(characterz);

        // Get all the characterzs
        restCharacterzMockMvc.perform(get("/api/characterzs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(characterz.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].player").value(hasItem(DEFAULT_PLAYER.toString())))
                .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
                .andExpect(jsonPath("$.[*].clazz").value(hasItem(DEFAULT_CLAZZ.toString())))
                .andExpect(jsonPath("$.[*].race").value(hasItem(DEFAULT_RACE.toString())))
                .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
                .andExpect(jsonPath("$.[*].lifePoints").value(hasItem(DEFAULT_LIFE_POINTS)))
                .andExpect(jsonPath("$.[*].magicPoints").value(hasItem(DEFAULT_MAGIC_POINTS)))
                .andExpect(jsonPath("$.[*].itens").value(hasItem(DEFAULT_ITENS.toString())))
                .andExpect(jsonPath("$.[*].skills").value(hasItem(DEFAULT_SKILLS.toString())));
    }

    @Test
    @Transactional
    public void getCharacterz() throws Exception {
        // Initialize the database
        characterzRepository.saveAndFlush(characterz);

        // Get the characterz
        restCharacterzMockMvc.perform(get("/api/characterzs/{id}", characterz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(characterz.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.player").value(DEFAULT_PLAYER.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.clazz").value(DEFAULT_CLAZZ.toString()))
            .andExpect(jsonPath("$.race").value(DEFAULT_RACE.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.lifePoints").value(DEFAULT_LIFE_POINTS))
            .andExpect(jsonPath("$.magicPoints").value(DEFAULT_MAGIC_POINTS))
            .andExpect(jsonPath("$.itens").value(DEFAULT_ITENS.toString()))
            .andExpect(jsonPath("$.skills").value(DEFAULT_SKILLS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCharacterz() throws Exception {
        // Get the characterz
        restCharacterzMockMvc.perform(get("/api/characterzs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCharacterz() throws Exception {
        // Initialize the database
        characterzRepository.saveAndFlush(characterz);
        characterzSearchRepository.save(characterz);
        int databaseSizeBeforeUpdate = characterzRepository.findAll().size();

        // Update the characterz
        Characterz updatedCharacterz = characterzRepository.findOne(characterz.getId());
        updatedCharacterz
                .name(UPDATED_NAME)
                .player(UPDATED_PLAYER)
                .level(UPDATED_LEVEL)
                .clazz(UPDATED_CLAZZ)
                .race(UPDATED_RACE)
                .age(UPDATED_AGE)
                .lifePoints(UPDATED_LIFE_POINTS)
                .magicPoints(UPDATED_MAGIC_POINTS)
                .itens(UPDATED_ITENS)
                .skills(UPDATED_SKILLS);
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(updatedCharacterz);

        restCharacterzMockMvc.perform(put("/api/characterzs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(characterzDTO)))
                .andExpect(status().isOk());

        // Validate the Characterz in the database
        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeUpdate);
        Characterz testCharacterz = characterzs.get(characterzs.size() - 1);
        assertThat(testCharacterz.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCharacterz.getPlayer()).isEqualTo(UPDATED_PLAYER);
        assertThat(testCharacterz.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testCharacterz.getClazz()).isEqualTo(UPDATED_CLAZZ);
        assertThat(testCharacterz.getRace()).isEqualTo(UPDATED_RACE);
        assertThat(testCharacterz.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testCharacterz.getLifePoints()).isEqualTo(UPDATED_LIFE_POINTS);
        assertThat(testCharacterz.getMagicPoints()).isEqualTo(UPDATED_MAGIC_POINTS);
        assertThat(testCharacterz.getItens()).isEqualTo(UPDATED_ITENS);
        assertThat(testCharacterz.getSkills()).isEqualTo(UPDATED_SKILLS);

        // Validate the Characterz in ElasticSearch
        Characterz characterzEs = characterzSearchRepository.findOne(testCharacterz.getId());
        assertThat(characterzEs).isEqualToComparingFieldByField(testCharacterz);
    }

    @Test
    @Transactional
    public void deleteCharacterz() throws Exception {
        // Initialize the database
        characterzRepository.saveAndFlush(characterz);
        characterzSearchRepository.save(characterz);
        int databaseSizeBeforeDelete = characterzRepository.findAll().size();

        // Get the characterz
        restCharacterzMockMvc.perform(delete("/api/characterzs/{id}", characterz.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean characterzExistsInEs = characterzSearchRepository.exists(characterz.getId());
        assertThat(characterzExistsInEs).isFalse();

        // Validate the database is empty
        List<Characterz> characterzs = characterzRepository.findAll();
        assertThat(characterzs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCharacterz() throws Exception {
        // Initialize the database
        characterzRepository.saveAndFlush(characterz);
        characterzSearchRepository.save(characterz);

        // Search the characterz
        restCharacterzMockMvc.perform(get("/api/_search/characterzs?query=id:" + characterz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(characterz.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].player").value(hasItem(DEFAULT_PLAYER.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].clazz").value(hasItem(DEFAULT_CLAZZ.toString())))
            .andExpect(jsonPath("$.[*].race").value(hasItem(DEFAULT_RACE.toString())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].lifePoints").value(hasItem(DEFAULT_LIFE_POINTS)))
            .andExpect(jsonPath("$.[*].magicPoints").value(hasItem(DEFAULT_MAGIC_POINTS)))
            .andExpect(jsonPath("$.[*].itens").value(hasItem(DEFAULT_ITENS.toString())))
            .andExpect(jsonPath("$.[*].skills").value(hasItem(DEFAULT_SKILLS.toString())));
    }
}
