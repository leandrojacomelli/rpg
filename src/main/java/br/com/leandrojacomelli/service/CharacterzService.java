package br.com.leandrojacomelli.service;

import br.com.leandrojacomelli.service.dto.CharacterzDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Characterz.
 */
public interface CharacterzService {

    /**
     * Save a characterz.
     *
     * @param characterzDTO the entity to save
     * @return the persisted entity
     */
    CharacterzDTO save(CharacterzDTO characterzDTO);

    /**
     *  Get all the characterzs.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CharacterzDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" characterz.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CharacterzDTO findOne(Long id);

    /**
     *  Delete the "id" characterz.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the characterz corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CharacterzDTO> search(String query, Pageable pageable);
}
