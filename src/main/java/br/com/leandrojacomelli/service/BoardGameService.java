package br.com.leandrojacomelli.service;

import br.com.leandrojacomelli.service.dto.BoardGameDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing BoardGame.
 */
public interface BoardGameService {

    /**
     * Save a boardGame.
     *
     * @param boardGameDTO the entity to save
     * @return the persisted entity
     */
    BoardGameDTO save(BoardGameDTO boardGameDTO);

    /**
     *  Get all the boardGames.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BoardGameDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" boardGame.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    BoardGameDTO findOne(Long id);

    /**
     *  Delete the "id" boardGame.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the boardGame corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BoardGameDTO> search(String query, Pageable pageable);
}
