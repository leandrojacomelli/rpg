package br.com.leandrojacomelli.service;

import br.com.leandrojacomelli.service.dto.WeatherDTO;
import feign.Param;
import feign.RequestLine;

/**
 * Created by leandro on 9/19/16.
 */
public interface OpenWeatherAPI {

    @RequestLine("GET /weather?lat={lat}&lon={lon}&mode=json&units=metric&cnt=7&APPID=27f68989b44fc372401da2fc806ddfd9")
    WeatherDTO getCurrentWeather(@Param("lat") Double lat,@Param("lon") Double lon );
}
