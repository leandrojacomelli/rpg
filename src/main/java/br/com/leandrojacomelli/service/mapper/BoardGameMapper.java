package br.com.leandrojacomelli.service.mapper;

import br.com.leandrojacomelli.domain.*;
import br.com.leandrojacomelli.service.dto.BoardGameDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity BoardGame and its DTO BoardGameDTO.
 */
@Mapper(componentModel = "spring", uses = {CharacterzMapper.class, })
public interface BoardGameMapper {

    BoardGameDTO boardGameToBoardGameDTO(BoardGame boardGame);

    List<BoardGameDTO> boardGamesToBoardGameDTOs(List<BoardGame> boardGames);

    BoardGame boardGameDTOToBoardGame(BoardGameDTO boardGameDTO);

    List<BoardGame> boardGameDTOsToBoardGames(List<BoardGameDTO> boardGameDTOs);

    default Characterz characterzFromId(Long id) {
        if (id == null) {
            return null;
        }
        Characterz characterz = new Characterz();
        characterz.setId(id);
        return characterz;
    }
}
