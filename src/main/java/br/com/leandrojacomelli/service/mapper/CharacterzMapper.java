package br.com.leandrojacomelli.service.mapper;

import br.com.leandrojacomelli.domain.*;
import br.com.leandrojacomelli.service.dto.CharacterzDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Characterz and its DTO CharacterzDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CharacterzMapper {

    CharacterzDTO characterzToCharacterzDTO(Characterz characterz);

    List<CharacterzDTO> characterzsToCharacterzDTOs(List<Characterz> characterzs);

    @Mapping(target = "boardGames", ignore = true)
    Characterz characterzDTOToCharacterz(CharacterzDTO characterzDTO);

    List<Characterz> characterzDTOsToCharacterzs(List<CharacterzDTO> characterzDTOs);
}
