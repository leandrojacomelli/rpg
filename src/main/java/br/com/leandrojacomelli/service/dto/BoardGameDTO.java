package br.com.leandrojacomelli.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the BoardGame entity.
 */
public class BoardGameDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String summary;

    @NotNull
    private LocalDate startDate;


    private Set<CharacterzDTO> characterzs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Set<CharacterzDTO> getCharacterzs() {
        return characterzs;
    }

    public void setCharacterzs(Set<CharacterzDTO> characterzs) {
        this.characterzs = characterzs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardGameDTO boardGameDTO = (BoardGameDTO) o;

        if ( ! Objects.equals(id, boardGameDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "BoardGameDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", summary='" + summary + "'" +
            ", startDate='" + startDate + "'" +
            '}';
    }
}
