package br.com.leandrojacomelli.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Characterz entity.
 */
public class CharacterzDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String player;

    @NotNull
    private Integer level;

    @NotNull
    private String clazz;

    @NotNull
    private String race;

    @NotNull
    private Integer age;

    private Integer lifePoints;

    private Integer magicPoints;

    private String itens;

    private String skills;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public Integer getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(Integer lifePoints) {
        this.lifePoints = lifePoints;
    }
    public Integer getMagicPoints() {
        return magicPoints;
    }

    public void setMagicPoints(Integer magicPoints) {
        this.magicPoints = magicPoints;
    }
    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }
    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CharacterzDTO characterzDTO = (CharacterzDTO) o;

        if ( ! Objects.equals(id, characterzDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CharacterzDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", player='" + player + "'" +
            ", level='" + level + "'" +
            ", clazz='" + clazz + "'" +
            ", race='" + race + "'" +
            ", age='" + age + "'" +
            ", lifePoints='" + lifePoints + "'" +
            ", magicPoints='" + magicPoints + "'" +
            ", itens='" + itens + "'" +
            ", skills='" + skills + "'" +
            '}';
    }
}
