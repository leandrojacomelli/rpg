package br.com.leandrojacomelli.service;

import br.com.leandrojacomelli.service.dto.WeatherDTO;

/**
 * Created by leandro on 9/19/16.
 */
public interface OpenWeather {

    WeatherDTO getCurrentWeather(Double lat, Double lon);
}
