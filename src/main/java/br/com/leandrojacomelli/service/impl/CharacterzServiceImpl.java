package br.com.leandrojacomelli.service.impl;

import br.com.leandrojacomelli.service.CharacterzService;
import br.com.leandrojacomelli.domain.Characterz;
import br.com.leandrojacomelli.repository.CharacterzRepository;
import br.com.leandrojacomelli.repository.search.CharacterzSearchRepository;
import br.com.leandrojacomelli.service.dto.CharacterzDTO;
import br.com.leandrojacomelli.service.mapper.CharacterzMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Characterz.
 */
@Service
@Transactional
public class CharacterzServiceImpl implements CharacterzService{

    private final Logger log = LoggerFactory.getLogger(CharacterzServiceImpl.class);
    
    @Inject
    private CharacterzRepository characterzRepository;

    @Inject
    private CharacterzMapper characterzMapper;

    @Inject
    private CharacterzSearchRepository characterzSearchRepository;

    /**
     * Save a characterz.
     *
     * @param characterzDTO the entity to save
     * @return the persisted entity
     */
    public CharacterzDTO save(CharacterzDTO characterzDTO) {
        log.debug("Request to save Characterz : {}", characterzDTO);
        Characterz characterz = characterzMapper.characterzDTOToCharacterz(characterzDTO);
        characterz = characterzRepository.save(characterz);
        CharacterzDTO result = characterzMapper.characterzToCharacterzDTO(characterz);
        characterzSearchRepository.save(characterz);
        return result;
    }

    /**
     *  Get all the characterzs.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<CharacterzDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Characterzs");
        Page<Characterz> result = characterzRepository.findAll(pageable);
        return result.map(characterz -> characterzMapper.characterzToCharacterzDTO(characterz));
    }

    /**
     *  Get one characterz by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CharacterzDTO findOne(Long id) {
        log.debug("Request to get Characterz : {}", id);
        Characterz characterz = characterzRepository.findOne(id);
        CharacterzDTO characterzDTO = characterzMapper.characterzToCharacterzDTO(characterz);
        return characterzDTO;
    }

    /**
     *  Delete the  characterz by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Characterz : {}", id);
        characterzRepository.delete(id);
        characterzSearchRepository.delete(id);
    }

    /**
     * Search for the characterz corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CharacterzDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Characterzs for query {}", query);
        Page<Characterz> result = characterzSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(characterz -> characterzMapper.characterzToCharacterzDTO(characterz));
    }
}
