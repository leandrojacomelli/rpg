package br.com.leandrojacomelli.service.impl;

import br.com.leandrojacomelli.service.OpenWeather;
import br.com.leandrojacomelli.service.OpenWeatherAPI;
import br.com.leandrojacomelli.service.dto.WeatherDTO;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by leandro on 9/19/16.
 */
@Service
public class OpenWeatherImpl implements OpenWeather {

    @Override
    public WeatherDTO getCurrentWeather(Double lat, Double lon) {
        OpenWeatherAPI api = Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .target(OpenWeatherAPI.class, "http://api.openweathermap.org/data/2.5");

        return api.getCurrentWeather(lat, lon);
    }
}
