package br.com.leandrojacomelli.service.impl;

import br.com.leandrojacomelli.service.BoardGameService;
import br.com.leandrojacomelli.domain.BoardGame;
import br.com.leandrojacomelli.repository.BoardGameRepository;
import br.com.leandrojacomelli.repository.search.BoardGameSearchRepository;
import br.com.leandrojacomelli.service.dto.BoardGameDTO;
import br.com.leandrojacomelli.service.mapper.BoardGameMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BoardGame.
 */
@Service
@Transactional
public class BoardGameServiceImpl implements BoardGameService{

    private final Logger log = LoggerFactory.getLogger(BoardGameServiceImpl.class);
    
    @Inject
    private BoardGameRepository boardGameRepository;

    @Inject
    private BoardGameMapper boardGameMapper;

    @Inject
    private BoardGameSearchRepository boardGameSearchRepository;

    /**
     * Save a boardGame.
     *
     * @param boardGameDTO the entity to save
     * @return the persisted entity
     */
    public BoardGameDTO save(BoardGameDTO boardGameDTO) {
        log.debug("Request to save BoardGame : {}", boardGameDTO);
        BoardGame boardGame = boardGameMapper.boardGameDTOToBoardGame(boardGameDTO);
        boardGame = boardGameRepository.save(boardGame);
        BoardGameDTO result = boardGameMapper.boardGameToBoardGameDTO(boardGame);
        boardGameSearchRepository.save(boardGame);
        return result;
    }

    /**
     *  Get all the boardGames.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<BoardGameDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BoardGames");
        Page<BoardGame> result = boardGameRepository.findAll(pageable);
        return result.map(boardGame -> boardGameMapper.boardGameToBoardGameDTO(boardGame));
    }

    /**
     *  Get one boardGame by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public BoardGameDTO findOne(Long id) {
        log.debug("Request to get BoardGame : {}", id);
        BoardGame boardGame = boardGameRepository.findOneWithEagerRelationships(id);
        BoardGameDTO boardGameDTO = boardGameMapper.boardGameToBoardGameDTO(boardGame);
        return boardGameDTO;
    }

    /**
     *  Delete the  boardGame by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BoardGame : {}", id);
        boardGameRepository.delete(id);
        boardGameSearchRepository.delete(id);
    }

    /**
     * Search for the boardGame corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BoardGameDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BoardGames for query {}", query);
        Page<BoardGame> result = boardGameSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(boardGame -> boardGameMapper.boardGameToBoardGameDTO(boardGame));
    }
}
