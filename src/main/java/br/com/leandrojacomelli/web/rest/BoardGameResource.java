package br.com.leandrojacomelli.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.leandrojacomelli.service.BoardGameService;
import br.com.leandrojacomelli.web.rest.util.HeaderUtil;
import br.com.leandrojacomelli.web.rest.util.PaginationUtil;
import br.com.leandrojacomelli.service.dto.BoardGameDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BoardGame.
 */
@RestController
@RequestMapping("/api")
public class BoardGameResource {

    private final Logger log = LoggerFactory.getLogger(BoardGameResource.class);
        
    @Inject
    private BoardGameService boardGameService;

    /**
     * POST  /board-games : Create a new boardGame.
     *
     * @param boardGameDTO the boardGameDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new boardGameDTO, or with status 400 (Bad Request) if the boardGame has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/board-games",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BoardGameDTO> createBoardGame(@Valid @RequestBody BoardGameDTO boardGameDTO) throws URISyntaxException {
        log.debug("REST request to save BoardGame : {}", boardGameDTO);
        if (boardGameDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("boardGame", "idexists", "A new boardGame cannot already have an ID")).body(null);
        }
        BoardGameDTO result = boardGameService.save(boardGameDTO);
        return ResponseEntity.created(new URI("/api/board-games/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("boardGame", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /board-games : Updates an existing boardGame.
     *
     * @param boardGameDTO the boardGameDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated boardGameDTO,
     * or with status 400 (Bad Request) if the boardGameDTO is not valid,
     * or with status 500 (Internal Server Error) if the boardGameDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/board-games",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BoardGameDTO> updateBoardGame(@Valid @RequestBody BoardGameDTO boardGameDTO) throws URISyntaxException {
        log.debug("REST request to update BoardGame : {}", boardGameDTO);
        if (boardGameDTO.getId() == null) {
            return createBoardGame(boardGameDTO);
        }
        BoardGameDTO result = boardGameService.save(boardGameDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("boardGame", boardGameDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /board-games : get all the boardGames.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of boardGames in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/board-games",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BoardGameDTO>> getAllBoardGames(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of BoardGames");
        Page<BoardGameDTO> page = boardGameService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/board-games");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /board-games/:id : get the "id" boardGame.
     *
     * @param id the id of the boardGameDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the boardGameDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/board-games/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BoardGameDTO> getBoardGame(@PathVariable Long id) {
        log.debug("REST request to get BoardGame : {}", id);
        BoardGameDTO boardGameDTO = boardGameService.findOne(id);
        return Optional.ofNullable(boardGameDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /board-games/:id : delete the "id" boardGame.
     *
     * @param id the id of the boardGameDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/board-games/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBoardGame(@PathVariable Long id) {
        log.debug("REST request to delete BoardGame : {}", id);
        boardGameService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("boardGame", id.toString())).build();
    }

    /**
     * SEARCH  /_search/board-games?query=:query : search for the boardGame corresponding
     * to the query.
     *
     * @param query the query of the boardGame search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/board-games",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BoardGameDTO>> searchBoardGames(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of BoardGames for query {}", query);
        Page<BoardGameDTO> page = boardGameService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/board-games");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
