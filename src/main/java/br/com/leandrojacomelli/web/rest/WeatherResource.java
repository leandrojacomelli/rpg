package br.com.leandrojacomelli.web.rest;

import br.com.leandrojacomelli.service.OpenWeather;
import br.com.leandrojacomelli.service.dto.WeatherDTO;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;

/**
 * REST controller for managing BoardGame.
 */
@RestController
@RequestMapping("/api")
public class WeatherResource {

    private final Logger log = LoggerFactory.getLogger(WeatherResource.class);

    @Inject
    private OpenWeather openWeather;


    /**
     * GET  /weather : get weather based on location.
     *
     * @param lat
     * @param lon
     * @return the ResponseEntity with status 200 (OK) and the current weather
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/weather",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WeatherDTO> getCurrentWeather(@RequestParam(value = "lat") Double lat, @RequestParam(value = "lon") Double lon)
        throws URISyntaxException {
        log.debug("REST request to get a page of get Current Weather");
        WeatherDTO currentWeather = openWeather.getCurrentWeather(lat, lon);

        return new ResponseEntity<>(currentWeather, HttpStatus.OK);
    }
}
