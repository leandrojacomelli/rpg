/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.leandrojacomelli.web.rest.vm;
