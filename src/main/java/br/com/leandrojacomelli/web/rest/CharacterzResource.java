package br.com.leandrojacomelli.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.leandrojacomelli.service.CharacterzService;
import br.com.leandrojacomelli.web.rest.util.HeaderUtil;
import br.com.leandrojacomelli.web.rest.util.PaginationUtil;
import br.com.leandrojacomelli.service.dto.CharacterzDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Characterz.
 */
@RestController
@RequestMapping("/api")
public class CharacterzResource {

    private final Logger log = LoggerFactory.getLogger(CharacterzResource.class);
        
    @Inject
    private CharacterzService characterzService;

    /**
     * POST  /characterzs : Create a new characterz.
     *
     * @param characterzDTO the characterzDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new characterzDTO, or with status 400 (Bad Request) if the characterz has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/characterzs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CharacterzDTO> createCharacterz(@Valid @RequestBody CharacterzDTO characterzDTO) throws URISyntaxException {
        log.debug("REST request to save Characterz : {}", characterzDTO);
        if (characterzDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("characterz", "idexists", "A new characterz cannot already have an ID")).body(null);
        }
        CharacterzDTO result = characterzService.save(characterzDTO);
        return ResponseEntity.created(new URI("/api/characterzs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("characterz", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /characterzs : Updates an existing characterz.
     *
     * @param characterzDTO the characterzDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated characterzDTO,
     * or with status 400 (Bad Request) if the characterzDTO is not valid,
     * or with status 500 (Internal Server Error) if the characterzDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/characterzs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CharacterzDTO> updateCharacterz(@Valid @RequestBody CharacterzDTO characterzDTO) throws URISyntaxException {
        log.debug("REST request to update Characterz : {}", characterzDTO);
        if (characterzDTO.getId() == null) {
            return createCharacterz(characterzDTO);
        }
        CharacterzDTO result = characterzService.save(characterzDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("characterz", characterzDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /characterzs : get all the characterzs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of characterzs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/characterzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CharacterzDTO>> getAllCharacterzs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Characterzs");
        Page<CharacterzDTO> page = characterzService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/characterzs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /characterzs/:id : get the "id" characterz.
     *
     * @param id the id of the characterzDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the characterzDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/characterzs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CharacterzDTO> getCharacterz(@PathVariable Long id) {
        log.debug("REST request to get Characterz : {}", id);
        CharacterzDTO characterzDTO = characterzService.findOne(id);
        return Optional.ofNullable(characterzDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /characterzs/:id : delete the "id" characterz.
     *
     * @param id the id of the characterzDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/characterzs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCharacterz(@PathVariable Long id) {
        log.debug("REST request to delete Characterz : {}", id);
        characterzService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("characterz", id.toString())).build();
    }

    /**
     * SEARCH  /_search/characterzs?query=:query : search for the characterz corresponding
     * to the query.
     *
     * @param query the query of the characterz search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/characterzs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CharacterzDTO>> searchCharacterzs(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Characterzs for query {}", query);
        Page<CharacterzDTO> page = characterzService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/characterzs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
