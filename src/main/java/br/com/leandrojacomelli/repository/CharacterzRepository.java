package br.com.leandrojacomelli.repository;

import br.com.leandrojacomelli.domain.Characterz;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Characterz entity.
 */
@SuppressWarnings("unused")
public interface CharacterzRepository extends JpaRepository<Characterz,Long> {

}
