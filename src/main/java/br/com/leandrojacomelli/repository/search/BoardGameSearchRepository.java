package br.com.leandrojacomelli.repository.search;

import br.com.leandrojacomelli.domain.BoardGame;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BoardGame entity.
 */
public interface BoardGameSearchRepository extends ElasticsearchRepository<BoardGame, Long> {
}
