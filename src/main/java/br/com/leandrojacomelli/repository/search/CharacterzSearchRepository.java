package br.com.leandrojacomelli.repository.search;

import br.com.leandrojacomelli.domain.Characterz;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Characterz entity.
 */
public interface CharacterzSearchRepository extends ElasticsearchRepository<Characterz, Long> {
}
