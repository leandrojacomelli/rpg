package br.com.leandrojacomelli.repository;

import br.com.leandrojacomelli.domain.BoardGame;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the BoardGame entity.
 */
@SuppressWarnings("unused")
public interface BoardGameRepository extends JpaRepository<BoardGame,Long> {

    @Query("select distinct boardGame from BoardGame boardGame left join fetch boardGame.characterzs")
    List<BoardGame> findAllWithEagerRelationships();

    @Query("select boardGame from BoardGame boardGame left join fetch boardGame.characterzs where boardGame.id =:id")
    BoardGame findOneWithEagerRelationships(@Param("id") Long id);

}
