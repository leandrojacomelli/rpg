package br.com.leandrojacomelli.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A BoardGame.
 */
@Entity
@Table(name = "board_game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "boardgame")
public class BoardGame implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "summary", nullable = false)
    private String summary;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "board_game_characterz",
               joinColumns = @JoinColumn(name="board_games_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="characterzs_id", referencedColumnName="ID"))
    private Set<Characterz> characterzs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public BoardGame name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public BoardGame summary(String summary) {
        this.summary = summary;
        return this;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public BoardGame startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Set<Characterz> getCharacterzs() {
        return characterzs;
    }

    public BoardGame characterzs(Set<Characterz> characterzs) {
        this.characterzs = characterzs;
        return this;
    }

    public BoardGame addCharacterz(Characterz characterz) {
        characterzs.add(characterz);
        characterz.getBoardGames().add(this);
        return this;
    }

    public BoardGame removeCharacterz(Characterz characterz) {
        characterzs.remove(characterz);
        characterz.getBoardGames().remove(this);
        return this;
    }

    public void setCharacterzs(Set<Characterz> characterzs) {
        this.characterzs = characterzs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BoardGame boardGame = (BoardGame) o;
        if(boardGame.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, boardGame.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "BoardGame{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", summary='" + summary + "'" +
            ", startDate='" + startDate + "'" +
            '}';
    }
}
