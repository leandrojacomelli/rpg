package br.com.leandrojacomelli.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Characterz.
 */
@Entity
@Table(name = "characterz")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "characterz")
public class Characterz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "player", nullable = false)
    private String player;

    @NotNull
    @Column(name = "level", nullable = false)
    private Integer level;

    @NotNull
    @Column(name = "clazz", nullable = false)
    private String clazz;

    @NotNull
    @Column(name = "race", nullable = false)
    private String race;

    @NotNull
    @Column(name = "age", nullable = false)
    private Integer age;

    @Column(name = "life_points")
    private Integer lifePoints;

    @Column(name = "magic_points")
    private Integer magicPoints;

    @Column(name = "itens")
    private String itens;

    @Column(name = "skills")
    private String skills;

    @ManyToMany(mappedBy = "characterzs")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BoardGame> boardGames = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Characterz name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlayer() {
        return player;
    }

    public Characterz player(String player) {
        this.player = player;
        return this;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Integer getLevel() {
        return level;
    }

    public Characterz level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getClazz() {
        return clazz;
    }

    public Characterz clazz(String clazz) {
        this.clazz = clazz;
        return this;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getRace() {
        return race;
    }

    public Characterz race(String race) {
        this.race = race;
        return this;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Integer getAge() {
        return age;
    }

    public Characterz age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getLifePoints() {
        return lifePoints;
    }

    public Characterz lifePoints(Integer lifePoints) {
        this.lifePoints = lifePoints;
        return this;
    }

    public void setLifePoints(Integer lifePoints) {
        this.lifePoints = lifePoints;
    }

    public Integer getMagicPoints() {
        return magicPoints;
    }

    public Characterz magicPoints(Integer magicPoints) {
        this.magicPoints = magicPoints;
        return this;
    }

    public void setMagicPoints(Integer magicPoints) {
        this.magicPoints = magicPoints;
    }

    public String getItens() {
        return itens;
    }

    public Characterz itens(String itens) {
        this.itens = itens;
        return this;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public String getSkills() {
        return skills;
    }

    public Characterz skills(String skills) {
        this.skills = skills;
        return this;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public Set<BoardGame> getBoardGames() {
        return boardGames;
    }

    public Characterz boardGames(Set<BoardGame> boardGames) {
        this.boardGames = boardGames;
        return this;
    }

    public Characterz addBoardGame(BoardGame boardGame) {
        boardGames.add(boardGame);
        boardGame.getCharacterzs().add(this);
        return this;
    }

    public Characterz removeBoardGame(BoardGame boardGame) {
        boardGames.remove(boardGame);
        boardGame.getCharacterzs().remove(this);
        return this;
    }

    public void setBoardGames(Set<BoardGame> boardGames) {
        this.boardGames = boardGames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Characterz characterz = (Characterz) o;
        if(characterz.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, characterz.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Characterz{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", player='" + player + "'" +
            ", level='" + level + "'" +
            ", clazz='" + clazz + "'" +
            ", race='" + race + "'" +
            ", age='" + age + "'" +
            ", lifePoints='" + lifePoints + "'" +
            ", magicPoints='" + magicPoints + "'" +
            ", itens='" + itens + "'" +
            ", skills='" + skills + "'" +
            '}';
    }
}
