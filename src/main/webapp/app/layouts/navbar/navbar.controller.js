(function () {
    'use strict';

    angular
        .module('rpgApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$state', '$window','$scope', 'Auth', 'Principal', 'ProfileService', 'LoginService', 'WeatherService'];

    function NavbarController($state, $window,$scope, Auth, Principal, ProfileService, LoginService, WeatherService) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.isAuthenticated = Principal.isAuthenticated;

        ProfileService.getProfileInfo().then(function (response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;
        $scope.weatherIcon = '';
        $scope.weatherTemperature = '';

        $window.navigator.geolocation.getCurrentPosition(function (position) {
            var location = {lat: position.coords.latitude, lon: position.coords.longitude};
            WeatherService.get(location,function(data){

                var icon = data.weather[0].icon;
                $scope.weatherIcon = 'http://openweathermap.org/img/w/'+icon+'.png';
                $scope.weatherTemperature = data.main.temp + ' °C';

            });

        });

        function login() {
            collapseNavbar();
            LoginService.open();
        }

        function logout() {
            collapseNavbar();
            Auth.logout();
            $state.go('home');
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }
    }
})();
