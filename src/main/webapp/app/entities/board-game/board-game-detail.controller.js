(function() {
    'use strict';

    angular
        .module('rpgApp')
        .controller('BoardGameDetailController', BoardGameDetailController);

    BoardGameDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'BoardGame', 'Characterz'];

    function BoardGameDetailController($scope, $rootScope, $stateParams, previousState, entity, BoardGame, Characterz) {
        var vm = this;

        vm.boardGame = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rpgApp:boardGameUpdate', function(event, result) {
            vm.boardGame = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
