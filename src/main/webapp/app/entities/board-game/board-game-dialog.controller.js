(function() {
    'use strict';

    angular
        .module('rpgApp')
        .controller('BoardGameDialogController', BoardGameDialogController);

    BoardGameDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BoardGame', 'Characterz'];

    function BoardGameDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BoardGame, Characterz) {
        var vm = this;

        vm.boardGame = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.characterzs = Characterz.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.boardGame.id !== null) {
                BoardGame.update(vm.boardGame, onSaveSuccess, onSaveError);
            } else {
                BoardGame.save(vm.boardGame, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rpgApp:boardGameUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.startDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
