(function() {
    'use strict';

    angular
        .module('rpgApp')
        .factory('CharacterzSearch', CharacterzSearch);

    CharacterzSearch.$inject = ['$resource'];

    function CharacterzSearch($resource) {
        var resourceUrl =  'api/_search/characterzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
