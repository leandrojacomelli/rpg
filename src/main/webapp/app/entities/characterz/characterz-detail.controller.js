(function() {
    'use strict';

    angular
        .module('rpgApp')
        .controller('CharacterzDetailController', CharacterzDetailController);

    CharacterzDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Characterz', 'BoardGame'];

    function CharacterzDetailController($scope, $rootScope, $stateParams, previousState, entity, Characterz, BoardGame) {
        var vm = this;

        vm.characterz = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rpgApp:characterzUpdate', function(event, result) {
            vm.characterz = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
