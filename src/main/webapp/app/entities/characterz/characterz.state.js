(function() {
    'use strict';

    angular
        .module('rpgApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('characterz', {
            parent: 'entity',
            url: '/characterz?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'rpgApp.characterz.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/characterz/characterzs.html',
                    controller: 'CharacterzController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('characterz');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('characterz-detail', {
            parent: 'entity',
            url: '/characterz/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'rpgApp.characterz.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/characterz/characterz-detail.html',
                    controller: 'CharacterzDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('characterz');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Characterz', function($stateParams, Characterz) {
                    return Characterz.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'characterz',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('characterz-detail.edit', {
            parent: 'characterz-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/characterz/characterz-dialog.html',
                    controller: 'CharacterzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Characterz', function(Characterz) {
                            return Characterz.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('characterz.new', {
            parent: 'characterz',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/characterz/characterz-dialog.html',
                    controller: 'CharacterzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                player: null,
                                level: null,
                                clazz: null,
                                race: null,
                                age: null,
                                lifePoints: null,
                                magicPoints: null,
                                itens: null,
                                skills: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('characterz', null, { reload: 'characterz' });
                }, function() {
                    $state.go('characterz');
                });
            }]
        })
        .state('characterz.edit', {
            parent: 'characterz',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/characterz/characterz-dialog.html',
                    controller: 'CharacterzDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Characterz', function(Characterz) {
                            return Characterz.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('characterz', null, { reload: 'characterz' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('characterz.delete', {
            parent: 'characterz',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/characterz/characterz-delete-dialog.html',
                    controller: 'CharacterzDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Characterz', function(Characterz) {
                            return Characterz.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('characterz', null, { reload: 'characterz' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
