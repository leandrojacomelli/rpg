(function() {
    'use strict';
    angular
        .module('rpgApp')
        .factory('Characterz', Characterz);

    Characterz.$inject = ['$resource'];

    function Characterz ($resource) {
        var resourceUrl =  'api/characterzs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
