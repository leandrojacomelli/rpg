(function() {
    'use strict';

    angular
        .module('rpgApp')
        .controller('CharacterzDialogController', CharacterzDialogController);

    CharacterzDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Characterz', 'BoardGame'];

    function CharacterzDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Characterz, BoardGame) {
        var vm = this;

        vm.characterz = entity;
        vm.clear = clear;
        vm.save = save;
        vm.boardgames = BoardGame.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.characterz.id !== null) {
                Characterz.update(vm.characterz, onSaveSuccess, onSaveError);
            } else {
                Characterz.save(vm.characterz, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rpgApp:characterzUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
