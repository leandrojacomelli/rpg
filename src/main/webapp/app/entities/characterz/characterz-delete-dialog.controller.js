(function() {
    'use strict';

    angular
        .module('rpgApp')
        .controller('CharacterzDeleteController',CharacterzDeleteController);

    CharacterzDeleteController.$inject = ['$uibModalInstance', 'entity', 'Characterz'];

    function CharacterzDeleteController($uibModalInstance, entity, Characterz) {
        var vm = this;

        vm.characterz = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Characterz.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
