(function() {
    'use strict';

    angular
        .module('rpgApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
