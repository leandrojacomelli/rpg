(function () {
    'use strict';

    angular
        .module('rpgApp')
        .factory('WeatherService', WeatherService);

    WeatherService.$inject = ['$resource'];

    function WeatherService ($resource) {
        var service = $resource('/api/weather', {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });

        return service;
    }
})();
